package com.puboot.filetotxt;

import com.puboot.filetotxt.exception.File2TextResolveNotFondException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class File2TextResolveFactory {
    @Autowired
    private List<File2TextResolve> file2TextResolves;

    public File2TextResolve getFileResolve(String type) throws File2TextResolveNotFondException {
        File2TextResolve file2TextResolve = null;
        for (File2TextResolve resolve : file2TextResolves) {
            if (type.equals(resolve.getType())) {
                file2TextResolve = resolve;
            }
        }
        if (file2TextResolve == null) {

            throw new File2TextResolveNotFondException("resolve2text for '" + type + "' not fond;");

        }
        return file2TextResolve;
    }
}

package com.puboot.filetotxt.exception;

/**
 * @author Jasper Huang
 */
public class File2TextResolveException extends RuntimeException{
    public File2TextResolveException(String message) {
        super(message);
    }
}

package com.puboot.filetotxt.impl;


import com.puboot.filetotxt.File2TextResolve;
import com.puboot.filetotxt.utils.FileAnalysisTool;
import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;

@Component
public class Txt2TextResolve implements File2TextResolve {
    private static String TYPE = "txt";

    @Override
    public String resolve(File file) throws IOException {
        String text = "";
        try {
            String encoding = FileAnalysisTool.guessFileEncoding(file);
            text = new String(FileUtils.readFileToByteArray(file), encoding);
        } catch (Exception e) {
            text = new String(FileUtils.readFileToByteArray(file), "GBK");
            throw e;
        }
        return text;
    }

    @Override
    public String getType() {
        return TYPE;
    }
    // 获得txt文件编码方式

}

package com.puboot.filetotxt.impl;

import com.puboot.filetotxt.File2TextResolve;
import org.apache.commons.io.FileUtils;
import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;

@Component
public class Docx2TextResolve implements File2TextResolve {
    private static String TYPE = "docx";

    @Override
    public String resolve(File file) throws IOException {
        String text = "";

        XWPFDocument doc;

        XWPFWordExtractor workbook = null;
        try {
            doc = new XWPFDocument(FileUtils.openInputStream(file));
            workbook = new XWPFWordExtractor(doc);
            text = workbook.getText();
        } catch (IOException e) {
            throw e;
        } finally {
            if (workbook != null) {
                try {
                    workbook.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return text;
    }

    @Override
    public String getType() {
        return TYPE;
    }
}

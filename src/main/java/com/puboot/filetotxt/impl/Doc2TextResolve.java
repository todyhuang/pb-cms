package com.puboot.filetotxt.impl;

import com.puboot.filetotxt.File2TextResolve;
import org.apache.commons.io.FileUtils;
import org.apache.poi.hwpf.extractor.WordExtractor;
import org.apache.poi.poifs.filesystem.FileMagic;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

@Component
public class Doc2TextResolve implements File2TextResolve {
    private static String TYPE = "doc";

    @Override
    public String resolve(File file) throws IOException {
        String text = "";
        WordExtractor ex = null;
        try {
            InputStream is = FileMagic.prepareToCheckMagic(FileUtils.openInputStream(file));
            FileMagic fm = FileMagic.valueOf(is);
            if (fm != FileMagic.OLE2) {
                return  new String(FileUtils.readFileToByteArray(file));
            }
            ex = new WordExtractor(FileUtils.openInputStream(file));
            text = ex.getText();
        } catch (Exception e) {
           throw e;
        } finally {
            if (ex != null) {
                try {
                    ex.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return text;
    }

    @Override
    public String getType() {
        return TYPE;
    }

}

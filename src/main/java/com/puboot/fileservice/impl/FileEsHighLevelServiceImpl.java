package com.puboot.fileservice.impl;

import com.puboot.fileentity.BizArticleDocument;
import com.puboot.fileentity.SearchLib;
import com.puboot.filerepository.BizArticleRepository;
import com.puboot.fileservice.FileEsHighLevelService;
import com.puboot.fileservice.model.Jfile;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.CreateIndexResponse;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by kk on 2020/7/31.
 */
@Slf4j
@Service
public class FileEsHighLevelServiceImpl implements FileEsHighLevelService {
    public static final TimeValue TIME_VALUE = new TimeValue(60,TimeUnit.SECONDS);
    @Autowired
    private BizArticleRepository bizArticleRepository;
    @Autowired
    private RestHighLevelClient esClient;

    @Override
    public boolean createFileIndex(SearchLib searchLib) throws IOException {
        //创建索引
        CreateIndexRequest request = new CreateIndexRequest(searchLib.getEsIndex());
        request.settings(Settings.builder()
                .put("index.number_of_shards", 5)
                .put("index.number_of_replicas", 3)
        );
        request.setTimeout(TIME_VALUE);
        XContentBuilder mapping   = XContentFactory.jsonBuilder()
                        .startObject()
                        //设置之定义字段
                        .startObject("properties")
                        .startObject("name")
                        .field("type", "text").field("analyzer", "ik_max_word").field("search_analyzer", "ik_smart")
                        .endObject()
                        //设置数据类型
                        .startObject("path")
                        .field("type", "text")
                        .endObject()
                        .startObject("content")
                        .field("type", "text").field("analyzer", "ik_max_word").field("search_analyzer", "ik_smart")
                        .endObject()
                        .startObject("viewFix")
                        .field("type", "text")
                        .endObject()
                        .endObject()
                        .endObject();
        request.mapping(mapping);
        CreateIndexResponse createIndexResponse = esClient.indices().create(request, RequestOptions.DEFAULT);
        return createIndexResponse.isAcknowledged();
    }

    @Override
    public String addFile2ES(String esIndex, String content, String name, String path, String viewFix) throws IOException {
        bizArticleRepository.save(new BizArticleDocument(null,content,name,path,viewFix));
        return null;
    }

    @Override
    public String addFile2ES(String esIndex, String content, String name, String path, String viewFix, Integer id) throws IOException {
        bizArticleRepository.save(new BizArticleDocument(id,content,name,path,viewFix));
        return null;
    }

    @Override
    public String deleteFileFromEs(String esIndex, String rid) {
        bizArticleRepository.deleteById(rid);
        return null;
    }

    @Override
    public List<Jfile> findFileByPath(SearchLib searchLib, String path) {
        return null;
    }

    @Override
    public Jfile queryJfileByRid(SearchLib searchLib, String rid) {
        return null;
    }
}

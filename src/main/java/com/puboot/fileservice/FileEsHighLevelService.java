package com.puboot.fileservice;

import com.puboot.fileentity.SearchLib;
import com.puboot.fileservice.model.Jfile;

import java.io.IOException;
import java.util.List;

/**
 * Created by kk on 2020/7/31.
 */
public interface FileEsHighLevelService {
    boolean createFileIndex(SearchLib searchLib) throws IOException;
    String addFile2ES(String esIndex, String content, String name, String path, String viewFix) throws IOException;
    String addFile2ES(String esIndex, String content, String name, String path, String viewFix,Integer id) throws IOException;
    String deleteFileFromEs(String esIndex, String rid);

    List<Jfile> findFileByPath(SearchLib searchLib, String path);

    Jfile queryJfileByRid(SearchLib searchLib, String rid);
}

package com.puboot.fileservice.common;

import java.util.Map;

public interface BaseCallBack {
    public void doCallBack(Map<String, String> msg);
}

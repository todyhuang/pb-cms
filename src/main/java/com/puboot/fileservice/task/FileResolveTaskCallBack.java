package com.puboot.fileservice.task;


import com.puboot.fileservice.model.ResolveCallBackMsg;

public interface FileResolveTaskCallBack {
    void doCallBack(ResolveCallBackMsg msg);
}

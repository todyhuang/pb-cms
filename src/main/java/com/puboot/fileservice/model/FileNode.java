package com.puboot.fileservice.model;

import java.util.List;

/**
 * Created by wangbiao-019 on 2018/4/18.
 */
public class FileNode {
    String id;
 /*   String pId;*/
    String name;
    boolean open;   //是否展开
    boolean isParent;   //是否是父节点
    int fileCount;
    List<FileNode> sons;
    String parentId;

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
/*
    public String getpId() {
        return pId;
    }

    public void setpId(String pId) {
        this.pId = pId;
    }*/

    public boolean isParent() {
        return isParent;
    }

    public void setParent(boolean parent) {
        isParent = parent;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isOpen() {
        return open;
    }

    public void setOpen(boolean open) {
        this.open = open;
    }

    public boolean getIsParent() {
        return isParent;
    }

    public void setIsParent(boolean isParent) {
        this.isParent = isParent;
    }

    public int getFileCount() {
        return fileCount;
    }

    public void setFileCount(int fileCount) {
        this.fileCount = fileCount;
    }

    public List<FileNode> getSons() {
        return sons;
    }

    public void setSons(List<FileNode> sons) {
        this.sons = sons;
    }

    @Override
    public String toString() {
        return "{" +
                "\"id\":\"" + id +
                "\", \"pId\":\"" + parentId +
                "\", \"name\":\"" + name +
                "\", \"open\":\"" + open +
                "\", \"isParent\":\"" + isParent +
                "\"}";
    }

}


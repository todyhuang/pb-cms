package com.puboot.filerepository;

import com.puboot.fileentity.BizArticleDocument;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.List;

public interface BizArticleRepository extends ElasticsearchRepository<BizArticleDocument,String> {

//    @Query("{\"match\": {\"title\":{ \"query\": \"?0\",\"operator\":\"?1\"}}}")
//    List<Item> findByTitleOperator(String title, String operator);

}
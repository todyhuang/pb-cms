package com.puboot.fileentity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.puboot.module.admin.model.BizCategory;
import com.puboot.module.admin.model.BizTags;
import com.puboot.module.admin.vo.base.BaseVo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author Jasper Huang
 * @version V1.0
 * @date 2020年08月10日
 */
@Data
@Accessors(chain = true)
@Document(indexName = "esfile",shards = 5,replicas = 3)
@AllArgsConstructor
@NoArgsConstructor
public class BizArticleDocument  implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    private Integer id;
    @Field(type = FieldType.Text,analyzer = "ik_max_word",searchAnalyzer = "ik_smart")
    private String content;
    @Field(type = FieldType.Text,analyzer = "ik_max_word",searchAnalyzer = "ik_smart")
    private String name;
    @Field(type =FieldType.Text)
    private String path;
    @Field(type =FieldType.Text)
    private String viewFix;
}

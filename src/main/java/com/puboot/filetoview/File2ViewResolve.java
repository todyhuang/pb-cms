package com.puboot.filetoview;

import org.jodconverter.office.OfficeException;

import java.io.IOException;

public interface File2ViewResolve {
    /**
     * @param fromPath 源文件路径
     * @return view文件路径
     */
    String resolve(String fromPath, String toPath) throws OfficeException, IOException;

    boolean canResolve(String fileFix);

    String getviewFix();
}

package com.puboot.filetoview;

import com.puboot.filetoview.exception.File2ViewResolveNotFondException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class File2ViewResolveFactory {
    @Autowired
    private List<File2ViewResolve> file2ViewResolves;

    public File2ViewResolve getFile2ViewResolve(String fix) throws File2ViewResolveNotFondException {
        for (File2ViewResolve resolve : file2ViewResolves) {
            if (resolve.canResolve(fix)) {
                return resolve;
            }
        }
        throw new File2ViewResolveNotFondException("没有找到view解析器：类型'"+fix+"'请注意是否没有添加注解;");


    }

}

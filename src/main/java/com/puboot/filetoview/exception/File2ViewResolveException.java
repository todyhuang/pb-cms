package com.puboot.filetoview.exception;

public class File2ViewResolveException extends RuntimeException {
    public File2ViewResolveException(String s) {
        super(s);
    }

    public File2ViewResolveException() {
    }
}

package com.puboot.filetoview.impl;

import com.puboot.filetoview.File2ViewResolve;
import org.springframework.stereotype.Component;

@Component
public class Pdf2ViewResolve implements File2ViewResolve {
    private static String RESOLVE_LIST = ".pdf";
    private static String RESOLVE2FIX = "";

    @Override
    public String resolve(String fromPath,String toPath) {
        return fromPath;
    }

    @Override
    public boolean canResolve(String fileFix) {
        return RESOLVE_LIST.contains(fileFix.toLowerCase());

    }

    @Override
    public String getviewFix() {
        return RESOLVE2FIX;
    }
}

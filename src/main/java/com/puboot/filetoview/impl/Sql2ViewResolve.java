package com.puboot.filetoview.impl;

import org.springframework.stereotype.Component;

@Component
public class Sql2ViewResolve extends Txt2ViewResolve {
    private static String RESOLVE_LIST = ".sql";
    private static String RESOLVE2FIX = "sql";
    @Override
    public boolean canResolve(String fileFix) {
        return RESOLVE_LIST.contains(fileFix.toLowerCase());

    }

    @Override
    public String getviewFix() {
        return RESOLVE2FIX;
    }
}

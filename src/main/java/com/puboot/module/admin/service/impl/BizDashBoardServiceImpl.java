package com.puboot.module.admin.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.puboot.module.admin.mapper.BizArticleLookMapper;
import com.puboot.module.admin.mapper.BizCommentMapper;
import com.puboot.module.admin.mapper.BizDashBoardMapper;
import com.puboot.module.admin.model.BizArticleLook;
import com.puboot.module.admin.model.BizDashBoard;
import com.puboot.module.admin.service.BizArticleLookService;
import com.puboot.module.admin.service.BizDashBoardService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * Created by kk on 2020/8/14.
 */
@Service
@AllArgsConstructor
public class BizDashBoardServiceImpl extends ServiceImpl<BizDashBoardMapper, BizDashBoard> implements BizDashBoardService {

    private final BizDashBoardMapper bizDashBoardMapper;
    @Override
    public BizDashBoard getBizDashBoardInfo() {
        return bizDashBoardMapper.getBizDashBoardInfo();
    }
}

package com.puboot.module.admin.service;

import java.util.Map;

/**
 * @author Jasper Huang
 * @version V1.0
 * @date 2020年08月10日
 */
@FunctionalInterface
public interface BizSiteInfoService {

    Map<String, Object> getSiteInfo();

}

package com.puboot.module.admin.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.puboot.common.annotation.Cache;
import com.puboot.module.admin.mapper.BizCategoryMapper;
import com.puboot.module.admin.model.BizCategory;
import com.puboot.module.admin.service.BizCategoryService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Jasper Huang
 * @version V1.0
 * @date 2020年08月10日
 */
@Service
@AllArgsConstructor
public class BizCategoryServiceImpl extends ServiceImpl<BizCategoryMapper, BizCategory> implements BizCategoryService {

    private final BizCategoryMapper bizCategoryMapper;

    @Override
    public List<BizCategory> selectCategories(BizCategory bizCategory) {
        return bizCategoryMapper.selectCategories(bizCategory);
    }

    @Override
    @Cache(flush = true)
    public int deleteBatch(Integer[] ids) {
        return bizCategoryMapper.deleteBatch(ids);
    }

    @Override
    public BizCategory selectById(Integer id) {
        return bizCategoryMapper.getById(id);
    }

    @Override
    public List<BizCategory> selectByPid(Integer pid) {
        return bizCategoryMapper.selectList(Wrappers.<BizCategory>lambdaQuery().eq(BizCategory::getPid, pid));
    }

    @Override
    @Cache
    public Map<String,String> selectAll(){
        List<BizCategory> bizCategories = bizCategoryMapper.selectAll();
        return bizCategories.stream().collect(Collectors.toMap(h -> h.getId().toString(), BizCategory::getName));
    }

    @Override
    @Cache(flush = true)
    public boolean save(BizCategory entity) {
        return super.save(entity);
    }

    @Override
    @Cache(flush = true)
    public boolean updateById(BizCategory entity) {
        return super.updateById(entity);
    }

    @Override
    @Cache(flush = true)
    public boolean removeById(Serializable id) {
        return super.removeById(id);
    }
}

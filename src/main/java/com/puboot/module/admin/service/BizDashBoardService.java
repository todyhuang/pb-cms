package com.puboot.module.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.puboot.module.admin.model.BizDashBoard;

/**
 * @author Jasper Huang
 * @version V1.0
 * @date 2020年08月10日
 */
public interface BizDashBoardService extends IService<BizDashBoard> {
    BizDashBoard getBizDashBoardInfo();
}

package com.puboot.module.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.puboot.module.admin.model.BizLove;
import org.apache.ibatis.annotations.Param;

/**
 * @author Jasper Huang
 * @version V1.0
 * @date 2020年08月10日
 */
public interface BizLoveMapper extends BaseMapper<BizLove> {

    BizLove checkLove(@Param("bizId") Integer bizId, @Param("userIp") String userIp);
}

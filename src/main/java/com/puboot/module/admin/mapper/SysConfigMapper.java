package com.puboot.module.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.puboot.module.admin.model.SysConfig;

/**
 * @author Jasper Huang
 * @version V1.0
 * @date 2020年08月10日
 */
public interface SysConfigMapper extends BaseMapper<SysConfig> {
}
package com.puboot.module.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.puboot.module.admin.model.BizArticleLook;
import com.puboot.module.admin.model.BizDashBoard;
import org.apache.ibatis.annotations.Param;

import java.util.Date;

/**
 * @author Jasper Huang
 * @version V1.0
 * @date 2020年08月10日
 */
public interface BizDashBoardMapper extends BaseMapper<BizDashBoard> {
    public BizDashBoard getBizDashBoardInfo();
}

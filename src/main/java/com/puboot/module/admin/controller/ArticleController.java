package com.puboot.module.admin.controller;

import cn.hutool.http.HtmlUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.puboot.common.util.CoreConst;
import com.puboot.common.util.Pagination;
import com.puboot.common.util.PushArticleUtil;
import com.puboot.common.util.ResultUtil;
import com.puboot.enums.SysConfigKey;
import com.puboot.fileentity.BizArticleDocument;
import com.puboot.fileentity.SearchLib;
import com.puboot.filerepository.BizArticleRepository;
//import com.puboot.fileservice.FileEsService;
import com.puboot.module.admin.model.BizArticle;
import com.puboot.module.admin.model.BizCategory;
import com.puboot.module.admin.model.BizTags;
import com.puboot.module.admin.model.User;
import com.puboot.module.admin.service.*;
import com.puboot.module.admin.vo.ArticleConditionVo;
import com.puboot.module.admin.vo.BaiduPushResVo;
import com.puboot.module.admin.vo.base.PageResultVo;
import com.puboot.module.admin.vo.base.ResponseVo;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 后台文章管理
 *
 * @author Jasper Huang
 * @version V1.0
 * @date 2020年08月10日
 */
@Controller
@RequestMapping("article")
@AllArgsConstructor
@Slf4j
public class ArticleController {

    private final BizArticleService articleService;
    private final BizArticleTagsService articleTagsService;
    private final BizCategoryService categoryService;
    private final BizTagsService tagsService;
    private final SysConfigService configService;
//    @Autowired
//    private FileEsService fileEsService;

    @Autowired
    private BizArticleRepository bizArticleRepository;

    @PostMapping("list")
    @ResponseBody
    public PageResultVo loadArticle(ArticleConditionVo articleConditionVo, Integer pageNumber, Integer pageSize) {
        articleConditionVo.setSliderFlag(true);
        IPage<BizArticle> page = new Pagination<>(pageNumber, pageSize);
        List<BizArticle> articleList = articleService.findByCondition(page, articleConditionVo);
        return ResultUtil.table(articleList, page.getTotal());
    }

    /*文章*/
    @GetMapping("/add")
    public String addArticle(Model model) {
        BizCategory bizCategory = new BizCategory();
        bizCategory.setStatus(CoreConst.STATUS_VALID);
        List<BizCategory> bizCategories = categoryService.selectCategories(bizCategory);
        List<BizTags> tags = tagsService.list();
        model.addAttribute("categories", bizCategories);
        model.addAttribute("tags", tags);
        BizArticle bizArticle = new BizArticle().setTags(new ArrayList<>()).setOriginal(1).setSlider(0).setTop(0).setRecommended(0).setComment(1);
        model.addAttribute("article", bizArticle);
        return CoreConst.ADMIN_PREFIX + "article/publish";
    }

    @PostMapping("/add")
    @ResponseBody
    @Transactional
    public ResponseVo add(BizArticle bizArticle, Integer[] tag) {
        try {
            User user = (User) SecurityUtils.getSubject().getPrincipal();
            bizArticle.setUserId(user.getUserId());
            bizArticle.setAuthor(user.getNickname());
            BizArticle article = articleService.insertArticle(bizArticle);
            articleTagsService.insertList(tag, article.getId());
            if(1==bizArticle.getStatus()){
                String jsonObject = JSONObject.toJSONString(bizArticle);
//                fileEsService.deleteFileFromEs("esfile",String.valueOf(article.getId()));
//                fileEsService.addFile2ES("esfile", bizArticle.getContent(), bizArticle.getTitle(),jsonObject ,"",article.getId());

                bizArticleRepository.deleteById(String.valueOf(article.getId()));
                 BizArticleDocument articleDocument = new BizArticleDocument(bizArticle.getId(),
                         HtmlUtil.cleanHtmlTag(article.getContent()),
                        bizArticle.getTitle(),jsonObject,"");
                bizArticleRepository.save(articleDocument);
            }
            return ResultUtil.success("保存文章成功");
        } catch (Exception e) {
            log.error("保存文章异常，原因:",e);
            return ResultUtil.error("保存文章失败");
        }
    }

    @GetMapping("/edit")
    public String edit(Model model, Integer id) {
        BizArticle bizArticle = articleService.selectById(id);
        model.addAttribute("article", bizArticle);
        BizCategory bizCategory = new BizCategory();
        bizCategory.setStatus(CoreConst.STATUS_VALID);
        List<BizCategory> bizCategories = categoryService.selectCategories(bizCategory);
        model.addAttribute("categories", bizCategories);
        List<BizTags> sysTags = tagsService.list();
        /*方便前端处理回显*/
        List<BizTags> aTags = new ArrayList<>();
        List<BizTags> sTags = new ArrayList<>();
        boolean flag;
        for (BizTags sysTag : sysTags) {
            flag = false;
            for (BizTags articleTag : bizArticle.getTags()) {
                if (articleTag.getId().equals(sysTag.getId())) {
                    BizTags tempTags = new BizTags();
                    tempTags.setId(sysTag.getId());
                    tempTags.setName(sysTag.getName());
                    aTags.add(tempTags);
                    sTags.add(tempTags);
                    flag = true;
                    break;
                }
            }
            if (!flag) {
                sTags.add(sysTag);
            }
        }
        bizArticle.setTags(aTags);
        model.addAttribute("tags", sTags);
        return CoreConst.ADMIN_PREFIX + "article/publish";
    }

    @PostMapping("/edit")
    @ResponseBody
    public ResponseVo edit(BizArticle article, Integer[] tag) throws IOException {
        articleService.updateById(article);
        articleTagsService.removeByArticleId(article.getId());
        articleTagsService.insertList(tag, article.getId());
        if(1==article.getStatus()){
            String jsonObject = JSONObject.toJSONString(article);
//            fileEsService.deleteFileFromEs("esfile",String.valueOf(article.getId()));
//            fileEsService.addFile2ES("esfile", article.getContent(), article.getTitle(),jsonObject ,"",article.getId());
            bizArticleRepository.deleteById(String.valueOf(article.getId()));
            BizArticleDocument articleDocument = new BizArticleDocument(article.getId(),
                   HtmlUtil.cleanHtmlTag(article.getContent()),
                    article.getTitle(),jsonObject,"");
            bizArticleRepository.save(articleDocument);
        }else if(0==article.getStatus()){
//            fileEsService.deleteFileFromEs("esfile",String.valueOf(article.getId()));
            bizArticleRepository.deleteById(String.valueOf(article.getId()));
        }
        return ResultUtil.success("编辑文章成功");
    }

    @PostMapping("/delete")
    @ResponseBody
    public ResponseVo delete(Integer id) {
        int i = articleService.deleteBatch(new Integer[]{id});
        try {
//            fileEsService.deleteFileFromEs("esfile",String.valueOf(id));
            bizArticleRepository.deleteById(String.valueOf(id));
        }catch (Exception e){

        }
         if (i > 0) {
            return ResultUtil.success("删除文章成功");
        } else {
            return ResultUtil.error("删除文章失败");
        }
    }

    @PostMapping("/batch/delete")
    @ResponseBody
    public ResponseVo deleteBatch(@RequestParam("ids[]") Integer[] ids) {
        int i = articleService.deleteBatch(ids);
        if (i > 0) {
            BizArticleDocument[] bizArticleDocument = new BizArticleDocument[ids.length];
            for(int index=0;index<bizArticleDocument.length;index++){
                BizArticleDocument tmp = new BizArticleDocument();
                tmp.setId(ids[index]);
                bizArticleDocument[index] = tmp;
            }
            Iterable<BizArticleDocument> iterable
                    = Arrays.asList(bizArticleDocument);
            bizArticleRepository.deleteAll(iterable);
            return ResultUtil.success("批量删除文章成功");
        } else {
            return ResultUtil.error("删除文章失败");
        }
    }

    @PostMapping("/batch/push")
    @ResponseBody
    public ResponseVo pushBatch(@RequestParam("urls[]") String[] urls) {
        try {
            String url = configService.selectAll().get(SysConfigKey.BAIDU_PUSH_URL.getValue());
            BaiduPushResVo baiduPushResVo = JSON.parseObject(PushArticleUtil.postBaidu(url, urls), BaiduPushResVo.class);
            if (baiduPushResVo.getNotSameSite() == null && baiduPushResVo.getNotValid() == null) {
                return ResultUtil.success("推送文章成功");
            } else {
                return ResultUtil.error("推送文章失败", baiduPushResVo);
            }
        } catch (Exception e) {
            return ResultUtil.error("推送文章失败,请检查百度推送接口！");
        }

    }



    //更新所有
    @PostMapping("/synarticle")
    @ResponseBody
    public ResponseVo synarticle() {
        ArticleConditionVo articleConditionVo = new ArticleConditionVo();
        articleConditionVo.setStatus(CoreConst.STATUS_VALID);
        List<BizArticle> articleList = articleService.findByCondition(articleConditionVo);
        for(BizArticle article:articleList){
            String jsonObject = JSONObject.toJSONString(article);
            BizArticleDocument articleDocument = new BizArticleDocument(article.getId(),
                    HtmlUtil.cleanHtmlTag(article.getContent()),
                    article.getTitle(),jsonObject,"");
            bizArticleRepository.save(articleDocument);
        }
       return ResultUtil.success("同步成功");
    }

}

package com.puboot.module.admin.vo.base;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

/**
 * @author Jasper Huang
 * @version V1.0
 * @date 2020年08月10日
 */
@Data
@AllArgsConstructor
public class PageResultVo {
    private List rows;
    private Long total;

}

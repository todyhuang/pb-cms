package com.puboot.module.admin.vo.base;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Jasper Huang
 * @version V1.0
 * @date 2020年08月10日
 */
@Data
public abstract class BaseVo implements Serializable {
    private static final long serialVersionUID = 1L;


    private Integer id;

    private Date createTime;
    private Date updateTime;

}
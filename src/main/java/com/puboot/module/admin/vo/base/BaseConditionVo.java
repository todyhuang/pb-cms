package com.puboot.module.admin.vo.base;

import lombok.Data;

/**
 * @author Jasper Huang
 * @version V1.0
 * @date 2020年08月10日
 */
@Data
public class BaseConditionVo {
    private int pageNumber = 1;
    private int pageSize = 10;

}

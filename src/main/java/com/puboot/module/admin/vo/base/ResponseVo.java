package com.puboot.module.admin.vo.base;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author Jasper Huang
 * @version V1.0
 * @date 2020年08月10日
 */
@Data
@AllArgsConstructor
public class ResponseVo<T> {

    private Integer status;
    private String msg;
    private T data;

    public ResponseVo(Integer status, String msg) {
        this.status = status;
        this.msg = msg;
    }

}

package com.puboot.module.admin.model;

import lombok.Data;

import java.io.Serializable;

/**
 * @author Jasper Huang
 * @version V1.0
 * @date 2020年08月10日
 */
@Data
public class SysConfig implements Serializable {

    private static final long serialVersionUID = -1645880315099183738L;

    private Integer id;

    /**
     * key
     */
    private String sysKey;

    /**
     * value
     */
    private String sysValue;

    /**
     * 状态  1：有效 0：无效
     */
    private Integer status;

    /**
     * 备注
     */
    private String remark;

}
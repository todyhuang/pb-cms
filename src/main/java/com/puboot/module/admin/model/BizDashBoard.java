package com.puboot.module.admin.model;

import lombok.Data;

import java.io.Serializable;

/**
 * Created by kk on 2020/8/14.
 */
@Data
public class BizDashBoard implements Serializable {
    //文章总数
    private Integer articlecounts;
    //评论总数
    private Integer commentcounts;
    //未发布文章数
    private Integer unpublishedcounts;
    //未审核评论数
    private Integer unauditcommentcounts;
    //标签总数
    private Integer tagscounts;

}

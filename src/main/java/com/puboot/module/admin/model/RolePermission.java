package com.puboot.module.admin.model;

import lombok.Data;

import java.io.Serializable;

/**
 * @author Jasper Huang
 * @version V1.0
 * @date 2020年08月10日
 */
@Data
public class RolePermission implements Serializable {

    private static final long serialVersionUID = -902800328539403089L;

    private Integer id;

    /**
     * 角色id
     */
    private String roleId;

    /**
     * 权限id
     */
    private String permissionId;

}
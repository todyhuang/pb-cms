//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.cksource.ckfinder.command;

import com.cksource.ckfinder.acl.Acl;
import com.cksource.ckfinder.annotation.RequiredMethod;
import com.cksource.ckfinder.config.Config;
import com.cksource.ckfinder.config.Config.Images;
import com.cksource.ckfinder.config.Config.ResourceType;
import com.cksource.ckfinder.filesystem.Backend;
import com.cksource.ckfinder.image.ImageSize;
import com.cksource.ckfinder.resourcetype.ResourceTypeFactory;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.util.UriUtils;

@RequiredMethod("GET")
public class Init implements Command {
    @Autowired
    private Config config;
    @Autowired
    Acl acl;
    @Autowired
    private ResourceTypeFactory resourceTypeFactory;
    private static final String CHARS = "123456789ABCDEFGHJKLMNPQRSTUVWXYZ";
    private static final int[] POSITIONS = new int[]{1, 8, 17, 22, 3, 13, 11, 20, 5, 24, 27};

    public Init() {
    }

    public ResponseEntity handle() throws IOException {
        Map<String, Object> data = new HashMap();
        data.put("enabled", Boolean.valueOf(true));
        String ln = "";
        String lc = this.config.getLicenseKey().replace("-", "");
        if(!lc.isEmpty()) {
            int pos = "123456789ABCDEFGHJKLMNPQRSTUVWXYZ".indexOf(lc.charAt(2)) % 5;
            if(pos == 1 || pos == 2) {
                ln = this.config.getLicenseName();
            }
        }

        data.put("s", ln);
        data.put("c", "FD6FHE82DWH");
        List<Object> resourceTypesData = new ArrayList();

        HashMap resourceTypeData;
        for(Iterator var5 = this.config.getResourceTypes().values().iterator(); var5.hasNext(); resourceTypesData.add(resourceTypeData)) {
            ResourceType resourceTypeConfig = (ResourceType)var5.next();
            com.cksource.ckfinder.resourcetype.ResourceType resourceType = this.resourceTypeFactory.getResourceType(resourceTypeConfig.getName());
            Backend backend = resourceType.getBackend();
            resourceTypeData = new HashMap();
            resourceTypeData.put("name", resourceType.getName());
            resourceTypeData.put("allowedExtensions", resourceType.getAllowedExtensionsAsString());
            resourceTypeData.put("deniedExtensions", resourceType.getDeniedExtensionsAsString());
            resourceTypeData.put("hash", resourceType.getHash());
            resourceTypeData.put("acl", Integer.valueOf(this.acl.check(resourceType, "/").getMask()));
            long maxSize = resourceType.getMaxSize();
            if(maxSize > 0L) {
                resourceTypeData.put("maxSize", Long.valueOf(maxSize));
            }

            resourceTypeData.put("hasChildren", Boolean.valueOf(backend.folderContainsSubfolders(resourceType)));
            if(backend.usesProxyCommand()) {
                resourceTypeData.put("useProxyCommand", Boolean.valueOf(true));
            } else {
                resourceTypeData.put("url", UriUtils.encodePath(resourceType.getUrl(), StandardCharsets.UTF_8));
            }

            if(resourceTypeConfig.isLazyLoaded()) {
                resourceTypeData.put("lazyLoad", Boolean.valueOf(true));
            }

            String label = resourceType.getLabel();
            if(label != null) {
                resourceTypeData.put("label", label);
            }
        }

        data.put("resourceTypes", resourceTypesData);
        data.put("uploadCheckImages", Boolean.valueOf(!this.config.checkSizeAfterScaling()));
        data.put("thumbs", this.getAllowedThumbnailSizes());
        data.put("images", this.getAllowedImageSizes());
        return ResponseEntity.ok(data);
    }

    private String processLicenseKey(String licenseKey) {
        StringBuilder stringBuilder = new StringBuilder();
        int[] var3 = POSITIONS;
        int var4 = var3.length;

        for(int var5 = 0; var5 < var4; ++var5) {
            int i = var3[var5];
            if(i < licenseKey.length()) {
                stringBuilder.append(licenseKey.charAt(i));
            }
        }

        return stringBuilder.toString();
    }

    private List<String> getAllowedThumbnailSizes() {
        List<String> sizes = new ArrayList();
        Iterator var2 = this.config.getThumbnailsConfig().getSizes().iterator();

        while(var2.hasNext()) {
            ImageSize imageSize = (ImageSize)var2.next();
            sizes.add(this.formatImageSize(imageSize.getWidth(), imageSize.getHeight()));
        }

        return sizes;
    }

    private Map<String, Object> getAllowedImageSizes() {
        Map<String, Object> imageSizes = new HashMap();
        Images imagesConfig = this.config.getImagesConfig();
        imageSizes.put("max", this.formatImageSize(imagesConfig.getMaxWidth(), imagesConfig.getMaxHeight()));
        Map<String, String> sizes = new HashMap();
        Iterator var4 = imagesConfig.getSizes().entrySet().iterator();

        while(var4.hasNext()) {
            Entry<String, ImageSize> entry = (Entry)var4.next();
            ImageSize size = (ImageSize)entry.getValue();
            sizes.put(entry.getKey(), this.formatImageSize(size.getWidth(), size.getHeight()));
        }

        imageSizes.put("sizes", sizes);
        return imageSizes;
    }

    private String formatImageSize(int width, int height) {
        return String.format("%dx%d", new Object[]{Integer.valueOf(width), Integer.valueOf(height)});
    }
}
